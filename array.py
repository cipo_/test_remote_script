import pandas as pd

def process(message):

    tmp = pd.DataFrame(message['Y'], columns=['Y'])
    tmp['order'] = tmp.index
    tmp['date_id'] = message['t0']
    
    return tmp.to_dict('records')

if __name__ == "__main__":

    import json

    f = open('test_okinsaid.json')

    message = json.load(f)

    print(message.keys())

    print(process(message=message))