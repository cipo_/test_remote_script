def process(message):
    result = {}

    result['t0'] = message['t0']
    result['dt'] = message['dt']
    result['id_geometria'] = message['ID geometria']
    result['reference'] = message['Reference']

    return result

if __name__ == "__main__":

    import json

    f = open('1buono.JSON')

    message = json.load(f)

    print(process(message=message))